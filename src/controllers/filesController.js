﻿const path = require('path');
const fs = require('fs/promises');
const express = require('express');

const filesController = express.Router();

const filesPath = path.join(__dirname, '../files');

// Костыли?
fs.access(filesPath).catch(
  () => {
    fs.mkdir(filesPath).catch();
  }
);

filesController.get('/', async (req, res, next) => {
  try {
    const files = await fs.readdir(filesPath);

    res.json({
      message: 'Success',
      files,
    })
  } catch (err) {
    console.error(err.message);
    res.status(500).json({
      message: 'Server error',
    });
  }
});

filesController.post('/', async (req, res, next) => {
  try {
    const { filename, content } = req.body;

    if (!content) {
      res.status(400).json({
        'message': "Please specify 'content' parameter"
      });
    } else if (!filename) {
      res.status(400).json({
        'message': "Please specify 'filename' parameter"
      });
    } else if (!filename.match(/[^:\/\\ /|/*\?><\"]*[\.](log|txt|json|yaml|xml|js|)$/)){
      res.status(400).json({
        'message': 'Only [log|txt|json|yaml|xml|js] file extensions are supported'
      });
    } else {
      await fs.writeFile(path.join(filesPath, filename), content);

      res.json({
        message: 'File created successfully',
      });
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).json({
      message: 'Server error',
    });
  }
});

filesController.get('/:filename', async (req, res, next) => {
  try {
    const { filename } = req.params;
    const pathToFile = path.join(filesPath, filename);
    const [, extension ] = path.extname(filename).split('.');

    // const stats = await fs.stat(pathToFile);
    // const content = await fs.readFile(pathToFile, 'utf-8');

    const [ stats, content ] = await Promise.all([
      fs.stat(pathToFile),
      fs.readFile(pathToFile, 'utf-8'),
    ]);

    res.json({
      message: 'Success',
      filename,
      content,
      extension,
      uploadedDate: stats.mtime,
    });
  } catch (err) {
    if (err.code === 'ENOENT') {
      const { filename } = req.params;
      res.status(400).json({
        message: `No file with '${filename}' filename found`,
      });
    } else {
      console.error(err.message);
      res.status(500).json({
        message: 'Server error',
      });
    }
  }
});

module.exports = {
  filesController,
};
