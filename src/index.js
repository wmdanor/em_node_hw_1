const express = require('express');
const morgan = require('morgan');

const app = express();

const { filesController } = require('./controllers/filesController');

const morganFormat = 'tiny';

app.use(express.json());
app.use(morgan(morganFormat));

app.use('/api/files', filesController);

app.use((err, req, res, next) => {
  console.error('Error caught', err);
  res.status(500).json({
    message: err.message,
  });
});

const start = async () => {
  try {
    app.listen(8080, () => {
      console.log('Server started');
    });
  } catch (err) {
    console.error('Failed to start the server: ', err.message);
  }
};

start();
